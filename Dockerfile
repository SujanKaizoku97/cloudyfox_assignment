FROM node:14

# Create app directory
WORKDIR /usr/src/app

ARG NODE_ENV=development
ARG PORT=5001
ENV PORT=$PORT

COPY package*.json ./

RUN yarn install


# Bundle app source
COPY . .

EXPOSE $PORT

CMD [ "yarn", "run","dev"]