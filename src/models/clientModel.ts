import {model,Schema,Model} from 'mongoose';
import ClientType from "../types/clientModelTypes";

const clientSchema:Schema = new Schema({
    name: {
        type: String,
        default: null
    },
    gender: {
        type: String,
        default: null
    },
    phone: {
        type: Number,
        default: null
    },
    email: {
        type: String,
        default: null
    },
    address: {
        type: String,
        default: null
    },
    nationality: {
        type: String,
        default: null
    },
    dob: {
        type:Date,
        default:null,
    },
    education_background : {
        type: String,
        default: null
    },
    preffered_mode : {
        type: String,
        default: null
    },
    created_at:{
        type: Date,
        default: new Date()
    },
    updated_at:{
        type: Date,
        default: new Date()
    }
})

const Client: Model<ClientType> = model('clients', clientSchema);

export default Client;