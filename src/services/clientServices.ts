import ClientModelType from "../types/clientModelTypes";
import ClientDataType from "../types/clientDataTypes";
import Client from "../models/clientModel";

const ClientServices:any ={};

ClientServices.findAll = async () : Promise<ClientModelType[]> => {
    const clients = await Client.find().sort("-created_at");
    return clients;
}

ClientServices.create = async (data:ClientDataType) =>{
    const client = await Client.create(data);
    return client;
}

ClientServices.findOne = async (query:any) : Promise<any> =>{
    const client = await Client.findOne(query);
    return client;
}

ClientServices.deleteOne = async (id:string) =>{
    const client = await Client.deleteOne({_id:id});
    return client;
}


ClientServices.findAndUpdate = async (id:string,data:ClientDataType)=>{
    const client = await Client.findByIdAndUpdate(id,data,{upsert:true,new:true});
    return client;
}

export default ClientServices;
