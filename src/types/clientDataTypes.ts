import ClientModelType from "./clientModelTypes";

export default interface ClientData{
    name:ClientModelType["name"],
    email:ClientModelType["email"],
    gender:ClientModelType["gender"],
    phone:ClientModelType["phone"],
    address:ClientModelType["address"],
    dob:ClientModelType["dob"],
    education_background:ClientModelType["education_background"],
    preffered_mode:ClientModelType["preffered_mode"],
    nationality:ClientModelType["nationality"],
    created_at:ClientModelType["created_at"],
    updated_at:ClientModelType["updated_at"],
}
