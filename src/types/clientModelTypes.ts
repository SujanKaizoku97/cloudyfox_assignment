import {Document} from 'mongoose';

export default interface ClientModelType extends Document{
    name:string,
    gender:string,
    phone:number,
    email:string,
    address:string,
    nationality:string
    dob:Date,
    education_background:string,
    preffered_mode:string,
    created_at:Date,
    updated_at:Date
}


