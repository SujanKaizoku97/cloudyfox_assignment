import {promisify} from "util";
const redis = require("redis");

const redisConfig = `redis://redis:6379/2`
const client = redis.createClient(redisConfig);

export const redisService  = {
  ...client,
  getAsync: promisify(client.get).bind(client),
  setAsync: promisify(client.set).bind(client),
  keysAsync: promisify(client.keys).bind(client),
  deleteAsync:promisify(client.del).bind(client)
};