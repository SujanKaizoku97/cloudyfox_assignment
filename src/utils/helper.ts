import ClientModelType from "../types/clientModelTypes";
const helperFunctions:any ={};
import {NextFunction, Request,Response } from "express";
import Exception from "./exceptionHandler";
import constants from "../constants/constants";
import ClientServices from "../services/clientServices";


helperFunctions.isEmpty = (value:any):boolean =>
    value === undefined ||
    value === null ||
    (typeof value === "object" && Object.keys(value).length === 0) ||
    (typeof value === "string" && value.trim().length === 0);

helperFunctions.alreadyExists =async (req:Request,res:Response,next:NextFunction)=>{
   try{
        const clientExists:ClientModelType = await ClientServices.findOne({email:req.body.email});
        if(clientExists){
            throw new Exception(400,constants.clientAlreadyExists);
        }else{
            next()
        }
   }catch(e){
       console.log(e);
       next(e);
   }
}

export default helperFunctions;

