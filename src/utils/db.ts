import mongoose from 'mongoose';

const dbConnect = () => {
  const url = "mongodb://mongo:27018/node";
  mongoose
    .connect(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    })
    .then(() => console.log("System Connected to the database"))
    .catch((e) => console.error(e));
};

export default dbConnect;