import {Router} from 'express';
import {addClient,getAllClients,getOneClient,updateOneClient,deleteOneClient} from "../controllers/clientController";
import ClientValidator from '../validation/clientAddValidation';
import helperFunctions from "../utils/helper";

const router = Router();

router.get("/clients",getAllClients);
router.post("/client",ClientValidator,helperFunctions.alreadyExists,addClient);
router.route("/client/:id").get(getOneClient).patch(updateOneClient).delete(deleteOneClient);

export default router;
