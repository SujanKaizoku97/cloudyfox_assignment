import {RequestHandler} from "express";
import helperFunctions from "../utils/helper";
import validator from 'validator';

const ClientValidator : RequestHandler = async (req,res,next)=>{
    const  errors :{[key:string]:string} ={};
    let {name,gender,phone,email,address,nationality,dob,education_background,preffered_mode} = req.body;
    name = !helperFunctions.isEmpty(name) ? name :"";
    gender = !helperFunctions.isEmpty(gender) ? gender :"";
    phone = !helperFunctions.isEmpty(phone) ? phone :"";
    email = !helperFunctions.isEmpty(email) ? email :"";
    address = !helperFunctions.isEmpty(address) ? address :"";
    nationality = !helperFunctions.isEmpty(nationality) ? nationality :"";
    education_background = !helperFunctions.isEmpty(education_background) ? education_background :"";
    dob = !helperFunctions.isEmpty(dob) ? dob :"";
    preffered_mode = !helperFunctions.isEmpty(preffered_mode) ? preffered_mode :"";

    if(validator.isEmpty(name)){
        errors.name ="Name field is required";
    }
    if(validator.isEmpty(gender)){
        errors.gender ="gender field is required";
    }
    if(validator.isEmpty(phone.toString())){
        errors.phone ="Phone field is required";
    }
    if(!validator.isMobilePhone(phone.toString())){
        errors.phone="Not a valid phone number";
    }

    if(validator.isEmpty(email)){
        errors.email ="Email field is required";
    }
    if(!validator.isEmail(email)){
        errors.email = "Not a valid email"
    }

    if(validator.isEmpty(address)){
        errors.address ="Address field is required";
    }
    if(validator.isEmpty(nationality)){
        errors.nationality ="Nationality field is required";
    }

    if(validator.isEmpty(dob)){
        errors.dob ="DOB field is required";
    }
    if(!validator.isDate(dob)){
        errors.dob ="Not a valid date"
    }

    if(validator.isEmpty(education_background)){
        errors.education_background ="Education Background field is required";
    }
    if(validator.isEmpty(preffered_mode)){
        errors.preffered_mode ="Preffered mode of contact field is required";
    }
    if(helperFunctions.isEmpty(errors)){
        next();
    }else{
        return res.json({
            errors,
        })
    }
}


export default ClientValidator;