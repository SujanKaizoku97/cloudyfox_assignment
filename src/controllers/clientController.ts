import {RequestHandler } from "express";
import {redisService} from "../utils/redisHandler";
import Exception from "../utils/exceptionHandler";
import ClientServices from "../services/clientServices";
import ClientModelType from "../types/clientModelTypes";
import ClientData from "../types/clientDataTypes";
import constants from "../constants/constants";

export const addClient : RequestHandler=async (req,res,next) :Promise <any> =>{
    try{
        const {name,gender,address,nationality,dob,education_background,preffered_mode}:ClientData = req.body;

        const email:ClientData["email"] = req.body.email ?? req.body.email.toLowerCase();
        const phone:ClientData["phone"] = req.body.phone ?? parseInt(req.body.phone,10);
        const addData:ClientData = {
            name,
            gender,
            phone,
            email,
            address,
            nationality,
            dob,
            education_background,
            preffered_mode,
            created_at:new Date(),
            updated_at:new Date()
        };

        const client:ClientModelType = await ClientServices.create(addData);
        await redisService.setAsync(client._id.toString(),JSON.stringify(client),'EX',10);
        return res.status(200).json({
            type:"success",
            client
        });
    }catch(e){
        console.log(e);
        next(e);
    }
}
export const getAllClients :RequestHandler=async (req,res,next):Promise <any>=>{
    try{
        const clients = await redisService.getAsync('clients');
        if(clients){
            console.log("using cached data");
            return res.status(200).json({
                type:"success",
                clients:JSON.parse(clients)
            });
        }
        const results:ClientModelType[] = await ClientServices.findAll();
        await redisService.setAsync('clients',JSON.stringify(results),'EX',5);
        return res.status(200).json({
            type:"success",
            clients:results
        });
    }catch(e){
        console.log(e);
        next(e);
    }
}

export const getOneClient :RequestHandler=async (req,res,next):Promise <any>=>{
    try{
        const client = await redisService.getAsync(req.params.id);
        if(client){
            console.log("using cached data");
            return res.status(200).json({
                type:"success",
                client:JSON.parse(client)
            });
        }
        const result:ClientModelType = await ClientServices.findOne({_id:req.params.id});
        if(!result){
            throw new Exception(400,constants.clientNotExistWithId);
        }
        await redisService.setAsync(req.params.id,JSON.stringify(result),'EX',10);
        return res.status(200).json({
            type:"success",
            client:result
        });
    }catch(e){
        console.log(e);
        next(e);
    }
}

export const updateOneClient :RequestHandler=async (req,res,next) :Promise <any> =>{
    try{
        const result:ClientModelType = await ClientServices.findOne({_id:req.params.id});
        if(!result){
            throw new Exception(400,constants.clientNotExistWithId);
        }
        const updatedData = {
            name: req.body.name ? req.body.name : result.name,
            gender: req.body.gender ? req.body.gender : result.gender,
            phone: req.body.phone ? parseInt(req.body.phone,10) : result.phone,
            email: req.body.email ? req.body.email : result.email,
            address: req.body.address ? req.body.address : result.address,
            nationality: req.body.nationality ? req.body.nationality : result.nationality,
            dob: req.body.dob ? req.body.dob : result.dob,
            education_background: req.body.education_background ? req.body.education_background : result.education_background,
            preffered_mode: req.body.preffered_mode ? req.body.preffered_mode : result.preffered_mode,
            updated_at:new Date()
        };
        const client  = await ClientServices.findAndUpdate(req.params.id,updatedData);
        await redisService.setAsync(req.params.id,JSON.stringify(client),'EX',10);
        return res.status(200).json({
            type:"success",
            client
        });
    }catch(e){
        console.log(e);
        next(e);
    }
}


export const deleteOneClient :RequestHandler=async (req,res,next):Promise <any>=>{
    try{
        const result:ClientModelType = await ClientServices.findOne({_id:req.params.id});
        if(!result){
            throw new Exception(400,constants.clientNotExistWithId);
        }
        await redisService.deleteAsync(req.params.id);
        await ClientServices.deleteOne(req.params.id);
        return res.status(200).json({
            type:"success",
            msg:constants.clientDeleteSuccessMsg
        })
    }catch(e){
        console.log(e);
        next(e);
    }
}

