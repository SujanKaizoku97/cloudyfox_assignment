import express,{Request,Response,NextFunction} from "express";
import clientRoutes from "./routes/clientRoutes";
import bodyParser from "body-parser";
import morgan from "morgan";
import cors from "cors";
import helmet from "helmet";
import Exception from "./utils/exceptionHandler";
import dbConnect from "./utils/db";


const app = express();

app.use(express.static("public"));
app.use(express.static("public/build"));

app.use(morgan("dev"));
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use(
    cors({
        origin:"http://localhost:3000"
    })
)
app.use((req, res, next)=>{
    res.setHeader(
      "Content-Security-Policy",
      "default-src * 'unsafe-inline' 'unsafe-eval' script-src 'self'  data: blob:; ",
    );
    return next();
});
app.use(helmet());
app.use("/api",clientRoutes);

app.use((err:Exception,req:Request,res:Response,next:NextFunction)=>{
    if (req.originalUrl.match(/(^|\W)api($|\W)/)) {
        res.status(err.status || 500).json({
            type:"error",
            msg:err.message || "Server Error"
        });
    } else {
        const errorStackTrace = process.env.NODE_ENV !== "production" ? err.stack : '';
        return res.render('500', {
            layout: false,
            errorStackTrace
        });
    }
});

// build react
app.get("*", (req, res) => {
    res.sendFile(require("path").resolve("./public/build/index.html"));
});

const port = 5001;

dbConnect();


app.listen( port, ():void => {
    console.log( `server started at http://localhost:${ port }` );
});

export default app;