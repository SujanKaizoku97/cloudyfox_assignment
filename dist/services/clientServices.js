"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const clientModel_1 = __importDefault(require("../models/clientModel"));
const ClientServices = {};
ClientServices.findAll = async () => {
    const clients = await clientModel_1.default.find().sort("-created_at");
    return clients;
};
ClientServices.create = async (data) => {
    const client = await clientModel_1.default.create(data);
    return client;
};
ClientServices.findOne = async (query) => {
    const client = await clientModel_1.default.findOne(query);
    return client;
};
ClientServices.deleteOne = async (id) => {
    const client = await clientModel_1.default.deleteOne({ _id: id });
    return client;
};
ClientServices.findAndUpdate = async (id, data) => {
    const client = await clientModel_1.default.findByIdAndUpdate(id, data, { upsert: true, new: true });
    return client;
};
exports.default = ClientServices;
