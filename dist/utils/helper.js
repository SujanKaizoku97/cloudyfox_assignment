"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const helperFunctions = {};
const exceptionHandler_1 = __importDefault(require("./exceptionHandler"));
const constants_1 = __importDefault(require("../constants/constants"));
const clientServices_1 = __importDefault(require("../services/clientServices"));
helperFunctions.isEmpty = (value) => value === undefined ||
    value === null ||
    (typeof value === "object" && Object.keys(value).length === 0) ||
    (typeof value === "string" && value.trim().length === 0);
helperFunctions.alreadyExists = async (req, res, next) => {
    try {
        const clientExists = await clientServices_1.default.findOne({ email: req.body.email });
        if (clientExists) {
            throw new exceptionHandler_1.default(400, constants_1.default.clientAlreadyExists);
        }
        else {
            next();
        }
    }
    catch (e) {
        console.log(e);
        next(e);
    }
};
exports.default = helperFunctions;
