"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.redisService = void 0;
const util_1 = require("util");
const redis = require("redis");
const redisConfig = `redis://redis:6379/2`;
const client = redis.createClient(redisConfig);
exports.redisService = {
    ...client,
    getAsync: util_1.promisify(client.get).bind(client),
    setAsync: util_1.promisify(client.set).bind(client),
    keysAsync: util_1.promisify(client.keys).bind(client),
    deleteAsync: util_1.promisify(client.del).bind(client)
};
