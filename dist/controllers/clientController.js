"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteOneClient = exports.updateOneClient = exports.getOneClient = exports.getAllClients = exports.addClient = void 0;
const redisHandler_1 = require("../utils/redisHandler");
const exceptionHandler_1 = __importDefault(require("../utils/exceptionHandler"));
const clientServices_1 = __importDefault(require("../services/clientServices"));
const constants_1 = __importDefault(require("../constants/constants"));
const addClient = async (req, res, next) => {
    var _a, _b;
    try {
        const { name, gender, address, nationality, dob, education_background, preffered_mode } = req.body;
        const email = (_a = req.body.email) !== null && _a !== void 0 ? _a : req.body.email.toLowerCase();
        const phone = (_b = req.body.phone) !== null && _b !== void 0 ? _b : parseInt(req.body.phone, 10);
        const addData = {
            name,
            gender,
            phone,
            email,
            address,
            nationality,
            dob,
            education_background,
            preffered_mode,
            created_at: new Date(),
            updated_at: new Date()
        };
        const client = await clientServices_1.default.create(addData);
        await redisHandler_1.redisService.setAsync(client._id.toString(), JSON.stringify(client), 'EX', 10);
        return res.status(200).json({
            type: "success",
            client
        });
    }
    catch (e) {
        console.log(e);
        next(e);
    }
};
exports.addClient = addClient;
const getAllClients = async (req, res, next) => {
    try {
        const clients = await redisHandler_1.redisService.getAsync('clients');
        if (clients) {
            console.log("using cached data");
            return res.status(200).json({
                type: "success",
                clients: JSON.parse(clients)
            });
        }
        const results = await clientServices_1.default.findAll();
        await redisHandler_1.redisService.setAsync('clients', JSON.stringify(results), 'EX', 5);
        return res.status(200).json({
            type: "success",
            clients: results
        });
    }
    catch (e) {
        console.log(e);
        next(e);
    }
};
exports.getAllClients = getAllClients;
const getOneClient = async (req, res, next) => {
    try {
        const client = await redisHandler_1.redisService.getAsync(req.params.id);
        if (client) {
            console.log("using cached data");
            return res.status(200).json({
                type: "success",
                client: JSON.parse(client)
            });
        }
        const result = await clientServices_1.default.findOne({ _id: req.params.id });
        if (!result) {
            throw new exceptionHandler_1.default(400, constants_1.default.clientNotExistWithId);
        }
        await redisHandler_1.redisService.setAsync(req.params.id, JSON.stringify(result), 'EX', 10);
        return res.status(200).json({
            type: "success",
            client: result
        });
    }
    catch (e) {
        console.log(e);
        next(e);
    }
};
exports.getOneClient = getOneClient;
const updateOneClient = async (req, res, next) => {
    try {
        const result = await clientServices_1.default.findOne({ _id: req.params.id });
        if (!result) {
            throw new exceptionHandler_1.default(400, constants_1.default.clientNotExistWithId);
        }
        const updatedData = {
            name: req.body.name ? req.body.name : result.name,
            gender: req.body.gender ? req.body.gender : result.gender,
            phone: req.body.phone ? parseInt(req.body.phone, 10) : result.phone,
            email: req.body.email ? req.body.email : result.email,
            address: req.body.address ? req.body.address : result.address,
            nationality: req.body.nationality ? req.body.nationality : result.nationality,
            dob: req.body.dob ? req.body.dob : result.dob,
            education_background: req.body.education_background ? req.body.education_background : result.education_background,
            preffered_mode: req.body.preffered_mode ? req.body.preffered_mode : result.preffered_mode,
            updated_at: new Date()
        };
        const client = await clientServices_1.default.findAndUpdate(req.params.id, updatedData);
        await redisHandler_1.redisService.setAsync(req.params.id, JSON.stringify(client), 'EX', 10);
        return res.status(200).json({
            type: "success",
            client
        });
    }
    catch (e) {
        console.log(e);
        next(e);
    }
};
exports.updateOneClient = updateOneClient;
const deleteOneClient = async (req, res, next) => {
    try {
        const result = await clientServices_1.default.findOne({ _id: req.params.id });
        if (!result) {
            throw new exceptionHandler_1.default(400, constants_1.default.clientNotExistWithId);
        }
        await redisHandler_1.redisService.deleteAsync(req.params.id);
        await clientServices_1.default.deleteOne(req.params.id);
        return res.status(200).json({
            type: "success",
            msg: constants_1.default.clientDeleteSuccessMsg
        });
    }
    catch (e) {
        console.log(e);
        next(e);
    }
};
exports.deleteOneClient = deleteOneClient;
