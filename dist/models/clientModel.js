"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const clientSchema = new mongoose_1.Schema({
    name: {
        type: String,
        default: null
    },
    gender: {
        type: String,
        default: null
    },
    phone: {
        type: Number,
        default: null
    },
    email: {
        type: String,
        default: null
    },
    address: {
        type: String,
        default: null
    },
    nationality: {
        type: String,
        default: null
    },
    dob: {
        type: Date,
        default: null,
    },
    education_background: {
        type: String,
        default: null
    },
    preffered_mode: {
        type: String,
        default: null
    },
    created_at: {
        type: Date,
        default: new Date()
    },
    updated_at: {
        type: Date,
        default: new Date()
    }
});
const Client = mongoose_1.model('clients', clientSchema);
exports.default = Client;
