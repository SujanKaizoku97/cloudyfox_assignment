"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const helper_1 = __importDefault(require("../utils/helper"));
const validator_1 = __importDefault(require("validator"));
const ClientValidator = async (req, res, next) => {
    const errors = {};
    let { name, gender, phone, email, address, nationality, dob, education_background, preffered_mode } = req.body;
    name = !helper_1.default.isEmpty(name) ? name : "";
    gender = !helper_1.default.isEmpty(gender) ? gender : "";
    phone = !helper_1.default.isEmpty(phone) ? phone : "";
    email = !helper_1.default.isEmpty(email) ? email : "";
    address = !helper_1.default.isEmpty(address) ? address : "";
    nationality = !helper_1.default.isEmpty(nationality) ? nationality : "";
    education_background = !helper_1.default.isEmpty(education_background) ? education_background : "";
    dob = !helper_1.default.isEmpty(dob) ? dob : "";
    preffered_mode = !helper_1.default.isEmpty(preffered_mode) ? preffered_mode : "";
    if (validator_1.default.isEmpty(name)) {
        errors.name = "Name field is required";
    }
    if (validator_1.default.isEmpty(gender)) {
        errors.gender = "gender field is required";
    }
    if (validator_1.default.isEmpty(phone.toString())) {
        errors.phone = "Phone field is required";
    }
    if (!validator_1.default.isMobilePhone(phone.toString())) {
        errors.phone = "Not a valid phone number";
    }
    if (validator_1.default.isEmpty(email)) {
        errors.email = "Email field is required";
    }
    if (!validator_1.default.isEmail(email)) {
        errors.email = "Not a valid email";
    }
    if (validator_1.default.isEmpty(address)) {
        errors.address = "Address field is required";
    }
    if (validator_1.default.isEmpty(nationality)) {
        errors.nationality = "Nationality field is required";
    }
    if (validator_1.default.isEmpty(dob)) {
        errors.dob = "DOB field is required";
    }
    if (!validator_1.default.isDate(dob)) {
        errors.dob = "Not a valid date";
    }
    if (validator_1.default.isEmpty(education_background)) {
        errors.education_background = "Education Background field is required";
    }
    if (validator_1.default.isEmpty(preffered_mode)) {
        errors.preffered_mode = "Preffered mode of contact field is required";
    }
    if (helper_1.default.isEmpty(errors)) {
        next();
    }
    else {
        return res.json({
            errors,
        });
    }
};
exports.default = ClientValidator;
