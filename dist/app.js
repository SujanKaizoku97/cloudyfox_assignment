"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const clientRoutes_1 = __importDefault(require("./routes/clientRoutes"));
const body_parser_1 = __importDefault(require("body-parser"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
const helmet_1 = __importDefault(require("helmet"));
const db_1 = __importDefault(require("./utils/db"));
const app = express_1.default();
app.use(express_1.default.static("public"));
app.use(express_1.default.static("public/build"));
app.use(morgan_1.default("dev"));
app.use(body_parser_1.default.urlencoded({ extended: true }));
app.use(body_parser_1.default.json());
app.use(cors_1.default({
    origin: "http://localhost:3000"
}));
app.use((req, res, next) => {
    res.setHeader("Content-Security-Policy", "default-src * 'unsafe-inline' 'unsafe-eval' script-src 'self'  data: blob:; ");
    return next();
});
app.use(helmet_1.default());
app.use("/api", clientRoutes_1.default);
app.use((err, req, res, next) => {
    if (req.originalUrl.match(/(^|\W)api($|\W)/)) {
        res.status(err.status || 500).json({
            type: "error",
            msg: err.message || "Server Error"
        });
    }
    else {
        const errorStackTrace = process.env.NODE_ENV !== "production" ? err.stack : '';
        return res.render('500', {
            layout: false,
            errorStackTrace
        });
    }
});
// build react
app.get("*", (req, res) => {
    res.sendFile(require("path").resolve("./public/build/index.html"));
});
const port = 5001;
db_1.default();
app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`);
});
exports.default = app;
