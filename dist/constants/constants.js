"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants = Object.freeze({
    clientAlreadyExists: "Client Already exists with that id",
    clientNotExistWithId: "No client exists with that id",
    clientDeleteSuccessMsg: "Client Deleted Successfully"
});
exports.default = constants;
