"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const supertest_1 = __importDefault(require("supertest"));
const app_1 = __importDefault(require("../app"));
describe("Testing API endpoints", () => {
    it("Getting all clients", async (done) => {
        const response = await supertest_1.default(app_1.default).get("/api/clients");
        const text = JSON.parse(response.text);
        expect(response.status).toEqual(200);
        expect(text.type).toEqual("success");
        expect(text).toHaveProperty("clients");
        done();
    });
    it("Getting One Client with wrong id", async () => {
        const id = "608d0a735e303200a7687388";
        const response = await supertest_1.default(app_1.default).get(`/api/client/${id}`);
        const text = JSON.parse(response.text);
        expect(response.status).toEqual(400);
        expect(text.type).toEqual("error");
        expect(text.msg).toEqual('No client exists with that id');
    });
    it("Adding Client", async () => {
        const addData = {
            name: "Hari Maya",
            gender: "Female",
            phone: '9841558245',
            email: "harimaya@gmail.com",
            address: "Baneshor",
            nationality: "Nepalese",
            dob: "2020-11-11",
            education_background: "BCA",
            preffered_mode: "Email",
        };
        const response = await supertest_1.default(app_1.default).post(`/api/client`).send(addData);
        const text = JSON.parse(response.text);
        expect(response.status).toEqual(200);
        expect(text.type).toEqual("success");
        expect(text).toHaveProperty("client");
    });
    it("Deleting One Client with wrong id", async () => {
        const id = "608d0a735e303200a7687388";
        const response = await supertest_1.default(app_1.default).delete(`/api/client/${id}`);
        const text = JSON.parse(response.text);
        expect(response.status).toEqual(400);
        expect(text.type).toEqual("error");
        expect(text.msg).toEqual('No client exists with that id');
    });
});
