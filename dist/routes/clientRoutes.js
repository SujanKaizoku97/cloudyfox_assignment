"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const clientController_1 = require("../controllers/clientController");
const clientAddValidation_1 = __importDefault(require("../validation/clientAddValidation"));
const helper_1 = __importDefault(require("../utils/helper"));
const router = express_1.Router();
router.get("/clients", clientController_1.getAllClients);
router.post("/client", clientAddValidation_1.default, helper_1.default.alreadyExists, clientController_1.addClient);
router.route("/client/:id").get(clientController_1.getOneClient).patch(clientController_1.updateOneClient).delete(clientController_1.deleteOneClient);
exports.default = router;
